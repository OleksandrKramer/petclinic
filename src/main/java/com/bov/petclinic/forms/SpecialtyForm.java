package com.bov.petclinic.forms;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpecialtyForm {
    private Long id;
    private String specialty;
}

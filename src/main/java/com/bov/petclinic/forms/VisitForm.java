package com.bov.petclinic.forms;

import java.util.Date;

public class VisitForm {

    private Long id;
    private Date visitDate;
    private String goalOfVisit;
    private Long pet;
    private Long owner;
}
